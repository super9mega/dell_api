use reqwest;
use serde::Deserialize;

#[derive(Deserialize, Clone)]
pub struct Asset {
    pub id: u128,
    pub serviceTag: String,
    pub orderBuid: u64,
    pub shipDate: String,
    pub productCode: String,
    pub localChannel: String,
    pub productLineDescription: String,
    pub productLobDescription: String,
    pub countryCode: String,
    pub duplicated: bool,
    pub invalid: bool
}

#[derive(Deserialize, Clone)]
pub struct AssetWithEntitlement {
    pub id: u64,
    pub serviceTag: String,
    pub orderBuid: u32,
    pub shipDate: String,
    pub productCode: String,
    pub localChannel: String,
    pub productLineDescription: String,
    pub productLobDescription: String,
    pub countryCode: String,
    pub duplicated: bool,
    pub invalid: bool,
    pub entitlements: Vec<Entitlement>
}

#[derive(Deserialize, Clone)]
pub struct Entitlement {
    pub itemNumber: String,
    pub startDate: String,
    pub endDate: String,
    pub entitlementType: String,
    pub serviceLevelCode: String,
    pub serviceLevelDescription: String,
    pub serviceLevelGroup: u32
}

#[derive(Deserialize, Clone)]
pub struct DellClient {
    pub access_token: String,
    pub token_type: String,
}

impl DellClient {
    pub fn new(client_id: &str, client_secret: &str,) -> Result<DellClient, reqwest::Error> {
        let token_url = "https://apigtwb2c.us.dell.com/auth/oauth/v2/token";
        let client = reqwest::blocking::Client::new();
        let body = [
            ("grant_type", "client_credentials"),
            ("client_id", client_id),
            ("client_secret", client_secret),
        ];

        let response = client
            .post(token_url)
            .form(&body)
            .send()?;

        let DellClient: DellClient = response.json()?;
        Ok(DellClient)
    }
    pub fn assets_from_servicetags(&self, service_tags: &str) -> Result<Vec<Asset>, reqwest::Error> {
        let asset_url = "https://apigtwb2c.us.dell.com/PROD/sbil/eapi/v5/assets";
        let client = reqwest::blocking::Client::new();

        let response = client
            .get(asset_url)
            .header("Authorization", format!("Bearer {}", &self.access_token))
            .header("Accept", "application/json")
            .query(&[("servicetags", service_tags)])
            .send()?;

        let asset: Vec<Asset> = response.json()?;
        Ok(asset)
    }
    pub fn assets_warranty_from_servicetags(&self, service_tags: &str) -> Result<Vec<AssetWithEntitlement>, reqwest::Error> {
        let asset_url = "https://apigtwb2c.us.dell.com/PROD/sbil/eapi/v5/asset-entitlements";
        let client = reqwest::blocking::Client::new();

        let response = client
            .get(asset_url)
            .header("Authorization", format!("Bearer {}", &self.access_token))
            .header("Accept", "application/json")
            .query(&[("servicetags", service_tags)])
            .send()?;

        let asset: Vec<AssetWithEntitlement> = response.json()?;
        Ok(asset)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn pull_servicetag() {
        let client = DellClient::new("l7b28d74ceecc04ebaab993d68e976da2c",
                                     "dbb4a4401aee4e89a21e24447fdd5af6").unwrap();
        let asset = client.assets_from_servicetags("1XBC524");
        println!("{}", asset.as_ref().unwrap()[0].productLineDescription);
        match asset {
            Ok(_) => {}
            Err(_) => {panic!("Did not return an access code")}
        }
    }
    #[test]
    fn pull_entitlements() {
        let client = DellClient::new("l7b28d74ceecc04ebaab993d68e976da2c",
                                     "dbb4a4401aee4e89a21e24447fdd5af6").unwrap();
        let asset = client.assets_warranty_from_servicetags("1XBC524");
        println!("{}", asset.as_ref().unwrap()[0].entitlements[0].endDate);
        match asset {
            Ok(_) => {}
            Err(_) => {panic!("Did not return an access code")}
        }
    }
}
